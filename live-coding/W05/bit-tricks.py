from z3 import *

x = BitVec('x', 32)
exp1 = And(x != 0, x & (x - 1) == 0)
print(exp1)

# exp2: x is a power of 2
powers = [2 ** i for i in range(32)]
print (powers)
exp2 = Or([x == p for p in powers])
# show that exp1 == exp1 is always true in our domain
# but the solver just returns one model!
# however, it's always true iff !(exp1 == exp2) is always false, i.e. unsat
sol = Solver()
sol.add(Not(exp1 == exp2))
print (sol.check())


# also experiment with
# exp1 = And (x & (x - 1) == 0)
# if you use that definition, you can do these:
#print (sol.model().eval(exp1))
#print (sol.model().eval(exp2))


